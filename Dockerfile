FROM golang:1.18-alpine as builder
LABEL authors="vyacheslav"
WORKDIR /app
COPY . .
RUN go build -o main
FROM alpine:latest
COPY --from=builder /app/main /main
CMD ["/main"]