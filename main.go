package main

import (
	"fmt"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strings"
)

type ReverseProxy struct {
	host string
	port string
}

func NewReverseProxy(host, port string) *ReverseProxy {
	return &ReverseProxy{
		host: host,
		port: port,
	}
}

// localhost:1313/static -> hugo
// localhost:1313/api -> api

func (rp *ReverseProxy) ReverseProxy(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.HasPrefix(r.URL.Path, "/api") {
			w.Write([]byte("Hello from API"))
			return
		}
		link := fmt.Sprintf("http://%s:%s", rp.host, rp.port)
		uri, err := url.Parse(link)
		if err != nil {
			http.Error(w, "Bad gateway", http.StatusBadGateway)
			return
		}

		if uri.Host == r.Host {
			next.ServeHTTP(w, r)
			return
		}

		proxy := httputil.ReverseProxy{Director: func(r *http.Request) {
			r.URL.Scheme = uri.Scheme
			r.URL.Host = uri.Host
			r.URL.Path = uri.Path + r.URL.Path
			r.Host = uri.Host
		}}
		r.Header.Set("Reverse-Proxy", "true")
		proxy.ServeHTTP(w, r)
	})
}

func main() {
	hugoProxy := NewReverseProxy("hugo", "1313")
	http.Handle("/", hugoProxy.ReverseProxy(http.DefaultServeMux))
	http.ListenAndServe(":1313", nil)

}
